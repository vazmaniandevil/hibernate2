package hibernateproject;


import java.util.*;

public class RunHibernateExample {

    public static void main(String[] args) {

        TestExample t = TestExample.getInstance();
        t.insertNewCar();

        //List is added after insert so that new entry is displayed as well
        List<Cars> c = t.showCars();
        for (Cars i : c) {
            System.out.println(i);
        }

        System.out.println("\nNew entry added successfully");
    }
}
