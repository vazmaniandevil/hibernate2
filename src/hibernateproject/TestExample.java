package hibernateproject;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.*;

public class TestExample {

    SessionFactory factory;
    Session session = null;
    Scanner scanner = new Scanner(System.in);

    private static TestExample single_instance = null;

    TestExample() {
        factory = HibernateUtil.getSessionFactory();
    }

    public static TestExample getInstance()
    {
        if (single_instance == null) {
            single_instance = new TestExample();
        }

        return single_instance;
    }

    public List<Cars> showCars() {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from hibernateproject.Cars";
            List<Cars> list = (List<Cars>)session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return list;

        } catch (Exception e) {
            System.out.println("Failed retrieving data");
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
            HibernateUtil.shutdown();
        }
    }

    public void insertNewCar() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        String make;
        String model;
        int year;

        System.out.print("Please, Enter the following information:\n");

        System.out.print("\n Make: ");
        make = scanner.nextLine();

        System.out.print("\n Model: ");
        model = scanner.nextLine();

        System.out.print("\n Year: ");
        year = Integer.parseInt(scanner.nextLine());

        try {
            Cars newCars = new Cars();
            newCars.setMake(make);
            newCars.setModel(model);
            newCars.setYear(year);

            session.save(newCars);
            session.getTransaction().commit();
        } catch (Exception e) {
            System.out.println("New entry could not be added");
        }
    }
}

